import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent} from './home/home.component';
import { RoomComponent } from './room/room.component';
import { RegisterComponent } from './register/register.component';
import { LadderComponent } from './ladder/ladder.component';
import { LoginComponent} from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component'

import { AuthGuard } from './_helpers';




const routes: Routes = [
  { path: '', component: HomeComponent},
   { path: 'room', component: RoomComponent, canActivate: [AuthGuard]  },
   { path: 'register', component: RegisterComponent },
   { path: 'ladder', component: LadderComponent, canActivate: [AuthGuard] },
   { path : 'login', component: LoginComponent},
   { path: '**', redirectTo: ''},
   //{ path: 'test', component: LoginComponent, outlet : "test-outlet"},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [  ]
})
export class AppRoutingModule { }
