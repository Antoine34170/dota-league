import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

import {Player} from '@dto/player';
import {Ladder} from '@dto/ladder';
import {environment} from '@environment/environment';

@Injectable({
  providedIn: 'root'
})
export class LadderService {

  constructor(private http: HttpClient) {
  }

  private ladderUrl = environment.apiUrl + '/ladder';
  player: Player;
  players: Player[];

  private log(log: string) {
    console.info(log);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  getLadder(): Observable<Ladder> {
    const url = `${this.ladderUrl}`;

    return this.http.get<Ladder>(url).pipe(
      tap(_ => this.log(`fetched ladder`)),
      catchError(this.handleError<Ladder>(`getLadder `))
    );
  }
}
