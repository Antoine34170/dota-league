import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SteamService {

  constructor(private http:HttpClient) { }

  private dotaUrl = 'https://api.opendota.com/api/players/';

  private log(log: string) {
    console.info(log);
  }

  //handle Error
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(`${operation} failed: ${error.message}`);

      return of(result as T)
    }
  }

  getPlayers(accountId : number) {

    const url = `${this.dotaUrl}/${accountId}`;
    return this.http.get<any>(url).pipe(
      tap(_ => this.log(`fetched steam-id steamID=${accountId}`)),
    catchError(this.handleError<any>(`getplayers id=${accountId}`)) 
    ) ;
  }

//



//

  
}
