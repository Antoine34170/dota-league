import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';


import {Injectable} from '@angular/core';
import {Login} from '../login/login';
import {environment} from '@environment/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  login: Login;

  private log(log: string) {
    console.info(log);
  }

  // handle Error
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }
/*
  isAuthenticated() {
    return this.http.get(`${environment.apiUrl}/auth/isAuthenticated`);
  }

*/
  /* LOGIN PUT
  logIn(login: Login): Observable<Login> {
    const url = `${environment.apiUrl}/login`;
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.post<Login>(url, login, httpOptions).pipe(
      tap(_ => this.log(`logged with =${login.login} ${url}`)),
      catchError(this.handleError<any>('login ok'))
    );
  }

*/

  /* LOGOUT GET
  logOut(login: Login): Observable<Login> {
    const url = `${environment.apiUrl}/logout`;
    return this.http.get<Login>(url).pipe(
      tap(_ => this.log(`logged with =${login.login} ${url}`)),
      catchError(this.handleError<any>('login ok'))
    );
  }
  */
}
