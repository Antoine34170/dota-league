import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError,  tap, retry } from 'rxjs/operators';

import { Register } from '../_dto/register';
import { environment } from '@environment/environment';



@Injectable({
  providedIn: 'root'
})
export class DataService {

  register : Register;

  constructor(private http: HttpClient) { }

  private log(log: string) {
    console.info(log);

}

private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
    console.log(error);
    console.log(`${operation} failed: ${error.message}`);

    return of(result as T);
  };
}

  


  signup(register : Register): Observable<Register> {
    console.log("test service");
    const url = `${environment.apiUrl}/register`;
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.post<Register>(url, register, httpOptions).pipe(
      tap(_ => this.log(`logged with =${register.username} ${url}`)),
      catchError(this.handleError<any>('login error'))
    );
  }











}
