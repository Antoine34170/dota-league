import { Injectable } from '@angular/core';

import { HttpClient} from '@angular/common/http';
import { Observable, of} from 'rxjs';
import { catchError, tap} from 'rxjs/operators';

import {Player} from '@dto/player';
import {Room} from "@dto/room";
import {environment} from "@environment/environment";

@Injectable({
  providedIn: 'root'
})
export class RoomService {

constructor(private http : HttpClient) { }

player : Player;
private roomUrl = environment.apiUrl + '/room';

private log(log: string) {
  console.info(log);
}

private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
    console.log(error);
    console.log(`${operation} failed: ${error.message}`);

    return of(result as T);
  };
}

joinRoom(): Observable<Room> {

  const url = `${this.roomUrl}/join`;

  return this.http.get<Room>(url).pipe(
    tap (_ => this.log(`fetched player`)),
    catchError(this.handleError<Room>(`getplayer `))
  );
}

leaveRoom(): Observable<Room> {
  const url = `${this.roomUrl}/leave`;
  return this.http.get<Room>(url).pipe(
    tap (_ => this.log(`fetched player`)),
    catchError(this.handleError<Room>(`getplayer `))
  );
}

displayRoom(): Observable<Room> {

  const url = `${this.roomUrl}/display`;

  return this.http.get<Room>(url).pipe(
    tap (_ => this.log(`fetched player`)),
    catchError(this.handleError<Room>(`getplayer `))
  );
}


}
