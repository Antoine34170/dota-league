import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@services/authentication.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService
  ) 
  { }

  ngOnInit(): void {
  }

  getJwtToken() : void{
    const user = this.authenticationService.currentUserValue;
    console.log(localStorage.getItem('jwtToken'));
    console.log("token : " + user.token);
    console.log("username : " + user.username);
  }
}
