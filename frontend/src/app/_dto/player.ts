export class Player {
  login: string;
  mmr: number;

  constructor(login, mmr) {

    this.login = login;
    this.mmr = mmr;
  }
}
