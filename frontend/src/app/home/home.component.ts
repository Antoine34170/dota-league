import { Component } from '@angular/core';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';






@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.css']
   })


export class HomeComponent {

    constructor() { }

    ngOnInit() {   }

    images = [
        "../../assets/dota-01.jpg",
        "../../assets/dota-05.jpg",
        "../../assets/dota-03.jpg"]

    image = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);
}
