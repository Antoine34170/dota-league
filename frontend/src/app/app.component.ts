import { Component } from '@angular/core';
import { Router } from '@angular/router';


import { AuthenticationService } from '@services/authentication.service';
import { User } from '@model/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Dota-League';
  currentUser: User;
  isLogged = false;

  constructor(
       private authenticationService: AuthenticationService,
       private router: Router
  ){ }

  ngOnInit(): void {
  {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    if (this.authenticationService.currentUserValue) {
                  this.isLogged = true;
              }
              else {
              this.isLogged = false;
              }
  }
}


 getJwtToken() : void {
    const user = this.authenticationService.currentUserValue;
    console.log(localStorage.getItem('jwtToken'));
    console.log("token : " + user.token);
    console.log("username : " + user.username);
  }

  logout(): void {
          this.authenticationService.logout();
          this.router.navigate(['/'])
      }



}
