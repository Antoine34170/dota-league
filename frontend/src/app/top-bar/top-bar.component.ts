import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {

  isLogged = false;

  constructor(
  private authenticationService: AuthenticationService,
  private router: Router
  ) {

  }

  ngOnInit(): void  {

if (this.authenticationService.currentUserValue) {
              this.isLogged = true;
          }
          else {
          this.isLogged = false;
          }
  }

  logout(): void {
            this.authenticationService.logout();
            this.router.navigate(['/'])
        }




}
