import { Component, OnInit } from '@angular/core';
import { Player } from '@dto/player';
import { LadderService } from '@services/ladder.service';


@Component({
  selector: 'app-ladder',
  templateUrl: './ladder.component.html',
  styleUrls: ['./ladder.component.css']
})
export class LadderComponent implements OnInit {


  players: Player[];
  mmrSortedPlayers: Player[];
  mmrSortedArray: boolean = false;
  ladder : Player[];


  constructor(private ladderService: LadderService) {
  }

  ngOnInit(): void {
    this.getLadder();


  }

  getLadder(): void {
    this.ladderService.getLadder()
      .subscribe(ladder => this.players = ladder.players);
  }

  getMmrSortedPlayers(): void {
    this.mmrSortedPlayers = this.players.sort((a, b) => a.mmr + b.mmr);
  }

  rankLadder(): void {
    this.players.sort((a, b) => a.mmr - b.mmr);
  }

  sortByMmr(): void {
    this.mmrSortedArray = !this.mmrSortedArray;
    if (this.mmrSortedArray) {
      this.players.sort((a, b) => a.mmr + b.mmr);
      console.log("clicked on true");
    } else if (!this.mmrSortedArray) {
      this.players.sort((a, b) => a.mmr - b.mmr);
      console.log("clicked on false");
    }
  }

//----------------






  selectEvent(item) {
    // do something with selected item
  }

  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e){
    // do something when input is focused
  }


}
