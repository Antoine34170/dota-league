import { Component, OnInit, PlatformRef } from '@angular/core';
import { RoomService } from '@services/room.service';
import { Player } from '@dto/player';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']

})
export class RoomComponent implements OnInit {

//  player: Player;
  players : Player[];
  room : Player[];


  constructor(private roomService : RoomService) { }




  ngOnInit(): void {
  this.displayRoom()
  }

  joinRoom():void {
    this.roomService.joinRoom().
    subscribe(room => this.players = room.players);

    console.log("joinRoom() checked")
  }

  leaveRoom():void{
    this.roomService.leaveRoom().
    subscribe(room => this.players = room.players);
    console.log("leaveRoom() checked")
  }

  displayRoom():void {
    this.roomService.displayRoom().
    subscribe(room => this.players = room.players);
    console.log("leaveRoom() checked")
  }






}
