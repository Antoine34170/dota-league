CREATE TABLE room_player(
    room_id UUID NOT NULL REFERENCES room(id) ,
    player_id UUID REFERENCES player(id),
    PRIMARY KEY (room_id, player_id)
);



insert into room (id, player_id)
values ('1c0b0f52-1ea3-11eb-adc1-0242ac120002','cc1fb8fb-7aa4-408a-9cff-595e487cf664'),
('1c0b0f52-1ea3-11eb-adc1-0242ac120002','5f33e011-5ec4-4b31-a668-893908803bf6'),
('1c0b0f52-1ea3-11eb-adc1-0242ac120002','82abdddc-91dc-4a34-bcb5-bfaf9468858e'),
('1c0b0f52-1ea3-11eb-adc1-0242ac120002','9a3f39e3-a258-4a32-a09e-9c2be9121141')
;