CREATE TABLE player(
    id UUID PRIMARY KEY,
    profile_name  varchar(50) NOT NULL default '' ,
    mmr INTEGER NOT NULL,
    user_id UUID REFERENCES users(id)
);

insert into player (id, profile_name, mmr, user_id)  values ('1c0b0d0e-1ea3-11eb-adc1-0242ac120002','FazerGerz','1337','94286a8b-6d23-4cec-b647-317b317ebb33');
