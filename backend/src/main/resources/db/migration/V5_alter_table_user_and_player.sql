ALTER TABLE player DROP COLUMN user_id;

ALTER TABLE users
ADD COLUMN player_id UUID REFERENCES player(id);