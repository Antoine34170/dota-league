CREATE TABLE room(
    id UUID PRIMARY KEY,
    created_at TIMESTAMPTZ NOT NULL DEFAULT Now() ,
    player_id UUID REFERENCES player(id)
);

insert into room (id, player_id)  values ('1c0b0f52-1ea3-11eb-adc1-0242ac120002','1c0b0d0e-1ea3-11eb-adc1-0242ac120002');

