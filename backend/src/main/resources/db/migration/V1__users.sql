CREATE TABLE users(
    id UUID PRIMARY KEY,
    username varchar(11) NOT NULL UNIQUE,
    password varchar(100) NOT NULL
);

insert into users (id, username, password)  values ('94286a8b-6d23-4cec-b647-317b317ebb33','test','$2a$10$QbGujJWm8PTnzFnclRtH5OW0tHrGqkEgrDV1caY3iPfARiKYmwrRi');

