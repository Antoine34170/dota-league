package com.elbonpais.gargleaguebackend.security

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<UserEntity, Long> {

    fun findById(id : Int) : UserEntity?
    fun findByUsername(username: String): UserEntity?




}