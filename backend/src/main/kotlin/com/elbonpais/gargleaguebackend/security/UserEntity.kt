package com.elbonpais.gargleaguebackend.security

import com.elbonpais.gargleaguebackend.dto.PlayerDto
import com.elbonpais.gargleaguebackend.jpa.PlayerEntity

import java.util.UUID
import javax.persistence.*


@Entity
@Table(name = "users")
data class UserEntity(
        @Id
        @GeneratedValue
        val id: UUID = UUID.randomUUID(),
        @Column
        val username: String,
        @Column
        val password: String,
        @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        @JoinColumn(name = "player_id")
        val playerEntity : PlayerEntity?
        )

/*
fun UserEntity.toPlayerEntity() : PlayerEntity {
        val id = UUID.randomUUID()
        val profilename = this.username
        val mmr = 1337
        val userEntity = this
        return PlayerEntity(id, profilename, mmr, userEntity)
}

fun UserEntity.toUserDto() : UserDTO {
        val username = this.username
        val password = this.password
        return UserDTO(username, password)

}

*/




