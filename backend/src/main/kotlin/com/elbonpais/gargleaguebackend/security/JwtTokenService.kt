package com.elbonpais.gargleaguebackend.security

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.util.*
import java.util.function.Function
import java.util.stream.Collectors

/**
 * This class service is used for generating the token,
 * extract the user info from token and also validating
 * the token and expiration as well.
 */
@Component
class JwtTokenService {
    @Value("\${jwt.secret:vnmrxxofzcvgzrizvgzhdrvftafhrytozplxcorihhevjgjhtmwcdltxekgkjfuuvnmrxxofzcvgzrizvgzhdrvftafhrytozplxcorihhevjgjhtmwcdltxekgkjfuu}")
    private val secret: String? = null

    //retrieve username from jwt token
    fun getUsernameFromToken(token: String?): String? {
        return getClaimFromToken(token, Function { obj: Claims -> obj.subject })
    }

    //retrieve expiration date from jwt token
    fun getExpirationDateFromToken(token: String?): Date {
        return getClaimFromToken(token, Function { obj: Claims -> obj.expiration })
    }

    fun getRoles(token: String?): List<*> {
        return getClaimFromToken(token, Function { claims: Claims -> claims[ROLES]  as List<*> })
    }

    fun <T> getClaimFromToken(token: String?, claimsResolver: Function<Claims, T>): T {
        val claims = getAllClaimsFromToken(token)
        return claimsResolver.apply(claims)
    }

    //for retrieving any information from token we will need the secret key
    private fun getAllClaimsFromToken(token: String?): Claims {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).body
    }

    //check if the token has expired
    private fun isTokenExpired(token: String?): Boolean {
        val expiration = getExpirationDateFromToken(token)
        return expiration.before(Date())
    }

    //generate token for user
    fun generateToken(authentication: Authentication): String {
        val claims: MutableMap<String, Any?> = HashMap()
        val user = authentication.principal as UserDetails
        val roles = authentication.authorities
                .stream()
                .map { obj: GrantedAuthority -> obj.authority }
                .collect(Collectors.toList())
        claims[ROLES] = roles
        return generateToken(claims, user.username)
    }

    //while creating the token -
    //1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
    //2. Sign the JWT using the HS512 algorithm and secret key.
    //3. According to JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
    //   compaction of the JWT to a URL-safe string
    private fun generateToken(claims: Map<String, Any?>, subject: String): String {
        val now = System.currentTimeMillis()
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(Date(now))
                .setExpiration(Date(now + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact()
    }

    //validate token
    fun validateToken(token: String?): Boolean {
        val username = getUsernameFromToken(token)
        return username != null && !isTokenExpired(token)
    }

    companion object {
        const val JWT_TOKEN_VALIDITY = 5 * 60 * 60.toLong()
        const val ROLES = "ROLES"
    }
}