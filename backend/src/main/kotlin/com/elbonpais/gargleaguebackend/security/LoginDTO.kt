package com.elbonpais.gargleaguebackend.security

data class LoginDTO(
        val token: String,
        val username : String
)