package com.elbonpais.gargleaguebackend.security


import java.util.*
import com.elbonpais.gargleaguebackend.security.UserEntity
import com.elbonpais.gargleaguebackend.jpa.PlayerEntity


data class UserDTO(
        val username: String,
        val password: String
)



