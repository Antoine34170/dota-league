package com.elbonpais.gargleaguebackend.security

import com.elbonpais.gargleaguebackend.jpa.PlayerEntity
import com.elbonpais.gargleaguebackend.jpa.PlayerRepository



import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

import java.util.UUID

@Service
class JwtUserDetailsService(
        private val userRepository: UserRepository,
        private val playerRepository: PlayerRepository,
        private val bcryptEncoder: PasswordEncoder
) : UserDetailsService {

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val (_, username1, password) = userRepository.findByUsername(username)
                ?: throw UsernameNotFoundException("User not found with username: $username")
        return User(username1, password,
                AuthorityUtils.createAuthorityList("ROLE_USER"))
    }

    //save new user
    fun save(user: UserDTO): UserEntity {

        val newUser = UserEntity(id = UUID.randomUUID(), username = user.username, password = bcryptEncoder.encode(user.password), playerEntity = PlayerEntity(UUID.randomUUID(),user.username,1337, null))

        /* todo save(newUser) > créer le player correspondant */

        return userRepository.save(newUser)
    }


}