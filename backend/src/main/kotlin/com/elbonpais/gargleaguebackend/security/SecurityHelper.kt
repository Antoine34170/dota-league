package com.elbonpais.gargleaguebackend.security

import com.elbonpais.gargleaguebackend.jpa.PlayerRepository
import org.springframework.security.core.context.SecurityContextHolder
import java.lang.IllegalStateException
import org.springframework.stereotype.Service


@Service
class SecurityHelper(
        private val playerRepository: PlayerRepository,
        private val userRepository: UserRepository
) {
    fun loggedUser() = userRepository.findByUsername(SecurityContextHolder.getContext().authentication.name) ?: throw IllegalStateException("Logged user not found in database")

    fun loggedPlayer() = playerRepository.findByProfilename(SecurityContextHolder.getContext().authentication.name) ?: throw IllegalStateException("Logged player not found in database")
}