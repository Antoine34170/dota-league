package com.elbonpais.gargleaguebackend.security

import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.MalformedJwtException
import org.flywaydb.core.internal.util.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.authentication.WebAuthenticationDetails
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import java.util.*
import java.util.stream.Collectors
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * This filter is used for validating the access_token sent by the user if it is valid or not.
 */
@Component
class JwtRequestFilter : OncePerRequestFilter() {
    @Autowired
    private val jwtTokenService: JwtTokenService? = null

    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        val jwt = getJwtFromRequest(request)
        jwt.ifPresent { token: String ->
            try {
                if (jwtTokenService!!.validateToken(token)) {
                    setSecurityContext(WebAuthenticationDetailsSource().buildDetails(request), token)
                }
            } catch (e: IllegalArgumentException) {
                logger.error("Unable to get JWT Token or JWT Token has expired")
                //UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken("anonymous", "anonymous", null);
                //SecurityContextHolder.getContext().setAuthentication(authentication);
            } catch (e: MalformedJwtException) {
                logger.error("Unable to get JWT Token or JWT Token has expired")
            } catch (e: ExpiredJwtException) {
                logger.error("Unable to get JWT Token or JWT Token has expired")
            }
        }
        filterChain.doFilter(request, response)
    }

    private fun setSecurityContext(authDetails: WebAuthenticationDetails, token: String) {
        val username = jwtTokenService!!.getUsernameFromToken(token)
        val roles = jwtTokenService.getRoles(token)
        val userDetails: UserDetails = User(username, "", roles.map { role -> SimpleGrantedAuthority(role as String) })
        val authentication = UsernamePasswordAuthenticationToken(userDetails, null,
                userDetails.authorities)
        authentication.details = authDetails
        // After setting the Authentication in the context, we specify
        // that the current user is authenticated. So it passes the
        // Spring Security Configurations successfully.
        SecurityContextHolder.getContext().authentication = authentication
    }

    companion object {
        private const val AUTHORIZATION = "Authorization"
        private const val BEARER = "Bearer "
        private fun getJwtFromRequest(request: HttpServletRequest): Optional<String> {
            val bearerToken = request.getHeader(AUTHORIZATION)
            return if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(BEARER)) {
                Optional.of(bearerToken.substring(7))
            } else Optional.empty()
        }
    }
}