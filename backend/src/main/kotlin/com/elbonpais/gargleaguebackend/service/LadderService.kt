package com.elbonpais.gargleaguebackend.service

import com.elbonpais.gargleaguebackend.dto.PlayerDto
import com.elbonpais.gargleaguebackend.dto.LadderDto
import com.elbonpais.gargleaguebackend.jpa.PlayerRepository
import com.elbonpais.gargleaguebackend.jpa.toPlayerDto
import org.springframework.stereotype.Service

@Service
class LadderService(
        private val playerRepository: PlayerRepository
        ) {

    fun findPlayersForLadder(): LadderDto {
        val allPlayers = playerRepository.findAll()
        val allPlayersDto = mutableSetOf<PlayerDto>()
        allPlayers.forEach {
            allPlayersDto.add(it.toPlayerDto())
        }
        return LadderDto(allPlayersDto.toList())
    }
}


