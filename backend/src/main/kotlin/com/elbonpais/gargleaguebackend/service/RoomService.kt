package com.elbonpais.gargleaguebackend.service

import com.elbonpais.gargleaguebackend.dto.PlayerDto
import com.elbonpais.gargleaguebackend.dto.RoomDto
import com.elbonpais.gargleaguebackend.jpa.PlayerEntity
import com.elbonpais.gargleaguebackend.jpa.PlayerRepository
import com.elbonpais.gargleaguebackend.jpa.RoomRepository
import com.elbonpais.gargleaguebackend.jpa.RoomEntity
import org.hibernate.JDBCException
import org.springframework.stereotype.Service
import java.util.*


@Service
class RoomService(
        private val roomRepository: RoomRepository,
        private val playerRepository: PlayerRepository
) {

    fun joinRoom(player: PlayerDto): RoomDto {
        val myCurrentRoom: RoomEntity = roomRepository.findByStatus("open")
        val myCurrentPlayer: PlayerEntity? = playerRepository.findByProfilename(player.login)
        val myPlayers: MutableSet<PlayerEntity> = myCurrentRoom.playerEntity.toMutableSet()
        if (myCurrentPlayer != null) {
            myPlayers.add(myCurrentPlayer)
        }
        myCurrentRoom.playerEntity = myPlayers.toList()
        roomRepository.save(myCurrentRoom)

        //roomReturned.add(player)
        val checkMyRoom = checkFullRoom(myCurrentRoom)
        if (checkMyRoom) {
            createNewRoom()
        }
        val roomReturned: MutableSet<PlayerDto> = returnRoom()
        return RoomDto(roomReturned.toList())
    }

    fun leaveRoom(player: PlayerDto): RoomDto {
        val myCurrentRoom: RoomEntity = roomRepository.findByStatus("open")
        val myCurrentPlayer: PlayerEntity = playerRepository.findByProfilename(player.login)
        val myPlayers: MutableSet<PlayerEntity> = myCurrentRoom.playerEntity.toMutableSet()
        myPlayers.remove(myCurrentPlayer)
        myCurrentRoom.playerEntity = myPlayers.toList()
        roomRepository.save(myCurrentRoom)
        val roomReturned: MutableSet<PlayerDto> = returnRoom()
        roomReturned.remove(player)
        return RoomDto(roomReturned.toList())
    }

    fun displayRooms(): RoomDto {
        return RoomDto(returnRoom().toList())
    }

    fun returnRoom(): MutableSet<PlayerDto> {
        val myRoomEntity = roomRepository.findByStatus("open")
        val listOfPlayersEntity = myRoomEntity.playerEntity
        val listOfPlayersDto = mutableSetOf<PlayerDto>()
        listOfPlayersEntity.forEach {
            val thisLogin = it.profilename
            val thisMmr = it.mmr
            val myPlayerDto = PlayerDto(thisLogin, thisMmr)
            listOfPlayersDto.add(myPlayerDto)
        }
        return listOfPlayersDto
    }

    fun checkFullRoom(room: RoomEntity): Boolean {
        if (room.playerEntity.size == 10) {
            room.status = "closed"
            return true
        }
        return false
    }

    fun createNewRoom() {
        val roomId = UUID.randomUUID()
        val created_at: Date = GregorianCalendar().time
        val status = "open"
        val playerEntity: List<PlayerEntity> = mutableListOf()
        val newRoom = RoomEntity(roomId, created_at, status, playerEntity.toList())
        roomRepository.save(newRoom)
    }


}