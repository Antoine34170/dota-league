package com.elbonpais.gargleaguebackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GargLeagueBackendApplication

fun main(args: Array<String>) {
    runApplication<GargLeagueBackendApplication>(*args)
}

