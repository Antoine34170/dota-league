package com.elbonpais.gargleaguebackend.jpa

import com.elbonpais.gargleaguebackend.security.UserEntity
import com.elbonpais.gargleaguebackend.dto.PlayerDto
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "player")
data class PlayerEntity (
        @Id
        @GeneratedValue
        val id: UUID = UUID.randomUUID(),
        @Column
        val profilename: String,
        @Column
        val mmr: Int = 0,
        /*@OneToOne(mappedBy = "playerEntity")
        var userEntity: UserEntity?,*/
        @ManyToMany(mappedBy = "playerEntity")
        var roomEntity: List<RoomEntity>? = mutableListOf()

)

fun PlayerEntity.toPlayerDto() : PlayerDto {
        val login = this.profilename;
        val mmr = this.mmr;
        return PlayerDto(login, mmr);

}









