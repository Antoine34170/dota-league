package com.elbonpais.gargleaguebackend.jpa


import org.hibernate.annotations.CreationTimestamp
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "room")
data class RoomEntity(
        @Id
        @GeneratedValue
        @Column
        val id: UUID = UUID.randomUUID(),
        @CreationTimestamp
        @Temporal(TemporalType.TIMESTAMP)
        @Column
        val created_at: Date,
        @Column(name = "status")
        var status: String,
        @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
        @JoinTable(name = "room_player",
                joinColumns = [JoinColumn(name = "room_id", referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "player_id", referencedColumnName = "id")])
        var playerEntity: List<PlayerEntity> = mutableListOf()
)

