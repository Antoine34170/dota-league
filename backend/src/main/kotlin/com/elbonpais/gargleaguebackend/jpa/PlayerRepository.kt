package com.elbonpais.gargleaguebackend.jpa

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PlayerRepository : JpaRepository<PlayerEntity, Long> {

    fun findByProfilename(login: String): PlayerEntity
    fun findAllByOrderByMmrAsc() : PlayerEntity

}

