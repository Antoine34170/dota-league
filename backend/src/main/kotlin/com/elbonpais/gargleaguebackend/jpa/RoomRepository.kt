package com.elbonpais.gargleaguebackend.jpa

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoomRepository : JpaRepository<RoomEntity, Long> {

    fun findByStatus(status: String): RoomEntity


}

