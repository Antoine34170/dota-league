package com.elbonpais.gargleaguebackend.controllers

import com.elbonpais.gargleaguebackend.dto.RoomDto
import com.elbonpais.gargleaguebackend.jpa.toPlayerDto
import com.elbonpais.gargleaguebackend.security.SecurityHelper
import com.elbonpais.gargleaguebackend.service.RoomService
import org.springframework.http.HttpStatus
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus

@APIController
@RequestMapping("/api/room")
@Secured("ROLE_USER")
class RoomController(
        private val roomService: RoomService,
        private val securityHelper: SecurityHelper
) {

    @GetMapping("/join")
    @ResponseStatus(HttpStatus.OK)
    fun join(): RoomDto {

        val currentPlayer = securityHelper.loggedPlayer().toPlayerDto()
        return roomService.joinRoom(currentPlayer)

    }

    @GetMapping("/leave")
    @ResponseStatus(HttpStatus.OK)
    fun leave(): RoomDto {
        var currentPlayer = securityHelper.loggedPlayer().toPlayerDto()
        return roomService.leaveRoom(currentPlayer)
    }

    @GetMapping("/display")
    @ResponseStatus(HttpStatus.OK)
    fun display(): RoomDto {
        return roomService.displayRooms()
    }
}
