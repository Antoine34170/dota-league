package com.elbonpais.gargleaguebackend.controllers

import com.elbonpais.gargleaguebackend.security.JwtTokenService
import com.elbonpais.gargleaguebackend.security.JwtUserDetailsService
import com.elbonpais.gargleaguebackend.security.LoginDTO
import com.elbonpais.gargleaguebackend.security.UserDTO
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@APIController
class AuthController(
        private val authenticationManager: AuthenticationManager,
        private val jwtTokenUtil: JwtTokenService,
        private val userDetailsService: JwtUserDetailsService
) {
    @RequestMapping(value = ["/auth/token"], method = [RequestMethod.POST])
    @Throws(Exception::class)
    fun createAuthenticationToken(@RequestBody authenticationRequest: UserDTO): ResponseEntity<*> {
        val auth = authenticate(authenticationRequest.username, authenticationRequest.password)
        SecurityContextHolder.getContext().authentication = auth
        return ResponseEntity.ok(LoginDTO(jwtTokenUtil.generateToken(auth), authenticationRequest.username))
    }

    @RequestMapping(value = ["/register"], method = [RequestMethod.POST])
    @Throws(Exception::class)
    fun saveUser(@RequestBody user: UserDTO): ResponseEntity<*> {
        return ResponseEntity.ok(userDetailsService.save(user))
    }

    @Throws(Exception::class)
    private fun authenticate(username: String, password: String): Authentication {
        return try {
            authenticationManager.authenticate(UsernamePasswordAuthenticationToken(username, password))
        } catch (e: DisabledException) {
            throw Exception("USER_DISABLED", e)
        } catch (e: BadCredentialsException) {
            throw Exception("INVALID_CREDENTIALS", e)
        }
    }
}