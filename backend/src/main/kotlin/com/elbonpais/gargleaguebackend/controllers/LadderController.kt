package com.elbonpais.gargleaguebackend.controllers

import com.elbonpais.gargleaguebackend.dto.LadderDto
import com.elbonpais.gargleaguebackend.service.LadderService
import org.springframework.http.HttpStatus
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus


@APIController
@RequestMapping("/api/ladder")
class LadderController(
        private val ladderService : LadderService
) {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Secured("ROLE_USER")
    fun getLadder(): LadderDto {

    return ladderService.findPlayersForLadder()

    }
}
