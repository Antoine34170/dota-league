package com.elbonpais.gargleaguebackend.dto

data class RegisterDto(
        val login: String,
        val password: String,
        val steamId: String
)