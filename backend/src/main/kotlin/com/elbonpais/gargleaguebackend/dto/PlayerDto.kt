package com.elbonpais.gargleaguebackend.dto

import com.elbonpais.gargleaguebackend.jpa.PlayerEntity
import com.elbonpais.gargleaguebackend.jpa.PlayerRepository

data class PlayerDto(
        val login: String,
        val mmr: Int
        )

