package com.elbonpais.gargleaguebackend.dto

data class RoomDto(
        val players: List<PlayerDto>

)

