package com.elbonpais.gargleaguebackend.dto

data class LadderDto(
        val players: List<PlayerDto>
)